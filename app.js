const express= require('express');
const path= require('path');
const https= require('https');
const mongoose=require('mongoose');
const bodyparser= require('body-parser');
const xmlparser= require('xml-js');
const app= express();

app.engine('html', require('ejs').renderFile);

const newsfeed= require('./model/news-feed');

app.set('views', path.join(__dirname, 'views'));
global.__basedir = __dirname;

mongoose.connect("mongodb+srv://ravi:ravi0807@sandbox-vyywo.mongodb.net/news_feed?retryWrites=true&w=majority")
.then(() => {
    console.log("connected to the database !");
})
.catch(() =>{
    console.log("connection failed!");
})

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));
    
https.get('https://timesofindia.indiatimes.com/rssfeedstopstories.cms', (resp) => {
    var bulkData=[];
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
        data += chunk;
    }); 

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
        var result= xmlparser.xml2json(data,{compact: true, spaces: 4});
        var JsonData = JSON.parse(result)
        for(let i=0; i<JsonData.rss.channel.item.length; i++){
            bulkData.push({title:JsonData.rss.channel.item[i].title._text,description:JsonData.rss.channel.item[i].description._text,link:JsonData.rss.channel.item[i].link._text,guid:JsonData.rss.channel.item[i].guid._text,pubDate: JsonData.rss.channel.item[i].pubDate._text})
        }
        newsfeed.insertMany(bulkData,(error, result)=>{
            if(error){
                return console.log(error.message);
            }else{
                console.log("multiple data inserted into db")
            }
        })
    });

    }).on("error", (err) => {
    console.log("Error: " + err.message);
    });

    app.get('', async (req, res)=>{
        res.render(__basedir + '/views/index.ejs')
    })

    app.get('/newsfeed/view',async (req,res)=>{
        const result = await newsfeed.find({},{title:1, description:1, link:1, guid:1, pubDate:1})
        res.render(__basedir + '/views/news-feed.ejs', {
            newsfeedResult : result
       });
    })










app.listen(6030,()=>{
    console.log("server is running on port 6030!!")
})